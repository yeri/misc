#!/bin/bash
hosts=(ocean liana tyr industry grima)

if [[ $1 = "" ]]; then

	for i in "${hosts[@]}"
	do :
		echo "	>> host: $i"
		echo "	>> updating smokeping"
		ssh -a $i 'cd git/smokeping/ ; ./start.sh'
		echo "	>> done for $i"
		echo ""
	done
else
	echo "	>> host: $1"
	echo "	>> updating smokeping"
	ssh -a $1 'cd git/smokeping/ ; ./start.sh'
	echo "	>> done for $1"
fi
