./configure --sbin-path=/usr/sbin/nginx --pid-path=/var/run/nginx.pid --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --with-http_stub_status_module --with-http_ssl_module --with-http_realip_module --with-http_gzip_static_module
make
make install
killall nginx
/etc/init.d/nginx restart

