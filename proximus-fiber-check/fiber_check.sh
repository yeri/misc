#!/bin/bash

if [ ! -f "addresses.csv" ]; then
	echo "Error: addresses.csv file not found."
	echo "Make sure there's a blank newline at the end."
	exit 1
fi

if [[ $0 == "./fiber_check.sh" ]]; then
	# color issues when using zsh and running ./fiber_check.sh
	echo "For best results, explicitly run this script using 'bash fiber_check.sh'"
	exit 1
fi

# Function to URL-encode a single string (for spaces)
function urlencode() {
	local string="${1// /%20}"
	echo "$string"
}

# Function to process the address and execute the curl command
function check_address() {
	# Use IFS to read CSV fields, remove trailing regular and non-breaking spaces
	IFS=, read -ra fields <<< "$(echo "$1" | sed 's/[[:space:] ]*$//')"

	straat="${fields[0]}"
	nummer="${fields[1]}"
	postcode="${fields[2]}"
	stad="${fields[3]}"

	# Encode necessary components 
	encoded_straat=$(urlencode "$straat")
	encoded_stad=$(urlencode "$stad")

	max_attempts=11
	attempts=1

	echo -n ">> Checking $straat $nummer $postcode $stad: "

	while [[ $attempts -lt $max_attempts ]]; do
		result=$(curl -sL -A "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:123.0) Gecko/20100101 Firefox/123.0" "https://snuru.mobilevikings.be/v1/products?street=$encoded_straat&number=$nummer&postcode=$postcode&city=$encoded_stad&country=be" | jq .)

		# debug
		#echo $result | jq
		
		if ! echo "$result" | jq -r '.message' | grep -q "Data not available yet. Try again later."; then

			max_download_speed=$(echo "$result" | jq -r '([.products[]?.product_definitions?.max_download_speed // null] | del(.[] | nulls) | max // "null")')


			if echo "$result" | jq -r '.message' | grep -q "Invalid address"; then
				echo "invalid address"

			elif echo "$result" | jq -r '.has_active_line' | grep -q "false" && [ "$max_download_speed" == "null" ]; then
				echo "no active internet lines"

			elif [[ $max_download_speed -eq 1000 || $max_download_speed -eq 5000 ]]; then

				echo -e "\e[32mFiber ($max_download_speed mbit)\e[0m"

			elif [[ $max_download_speed == "null" ]]; then
				echo "no active internet lines"

			else
				echo -e "\e[31mVDSL ($max_download_speed mbit)\e[0m"
			fi

			echo ""
			return
		fi

		echo "Data not available yet. Retrying (attempt #$attempts)..."
		sleep 10
		attempts=$(( attempts + 1 ))
	done

	# Reached maximum attempts
	echo "Failed to get data after $max_attempts attempts."
}

# Read addresses from the CSV file (skipping the header line)
while IFS= read -r address || [ -n "$address" ]; do
	check_address "$address"
	sleep 1
done < <(tail -n +2 addresses.csv)
