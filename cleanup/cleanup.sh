#!/bin/bash
find /var/log -name "*.gz" -exec rm -f {} +

if [ -d "/var/log/munin" ]; then
	rm -f /var/log/munin/*.1
fi