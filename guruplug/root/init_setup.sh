#!/bin/sh

# This is called from /etc/rc.local to perform the initial setup.

echo 1 > /proc/sys/net/ipv4/ip_forward

# We always bootup in AP mode. Delete any stale files
rm -f /etc/wlanclient.mode
SSID=Plug2-uAP-`ifconfig eth0 | awk -F ":" '/HWaddr/ {print $6$7}'`

insmod /root/uap8xxx.ko
ifconfig uap0 10.10.10.1 netmask 255.255.255.0 up
# WiFi
#/usr/bin/uaputl sys_cfg_ssid $SSID
/usr/bin/uaputl bss_stop
/usr/bin/uaputl sys_cfg_ssid 0x04
/usr/bin/uaputl sys_cfg_protocol 32
/usr/bin/uaputl sys_cfg_wpa_passphrase "bla"
/usr/bin/uaputl sys_cfg_cipher 8 8
/usr/bin/uaputl sys_cfg_channel 1
/usr/bin/uaputl bss_start

iptables -F
iptables -t nat -F

iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP

iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE

# Allow TUN interface connections to OpenVPN server
iptables -A INPUT -i tun+ -j ACCEPT
# Allow TUN interface connections to be forwarded through other interfaces
iptables -A FORWARD -i tun+ -j ACCEPT
iptables -A FORWARD -o tun+ -j ACCEPT
# Allow TUN interface connections to get out
iptables -A OUTPUT -o tun+ -j ACCEPT

iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A INPUT -i uap0 -p TCP --dport 22 -j ACCEPT
iptables -A INPUT -p TCP --dport 222 -j ACCEPT
iptables -A INPUT -p TCP --dport 2222 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 123 -j ACCEPT
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -i uap0 -j ACCEPT
iptables -A INPUT -i tun0 -j ACCEPT

# samba
#iptables -A -p udp -m udp --dport 137 -j ACCEPT
#iptables -A -p udp -m udp --dport 138 -j ACCEPT
#iptables -A	 -m state --state NEW -m tcp -p tcp --dport 139 -j ACCEPT
#iptables -A -m state --state NEW -m tcp -p tcp --dport 445 -j ACCEPT

# smtp
#iptables -A input -i eth0 -p tcp --dport 25 -j accept

iptables -I FORWARD -i uap0 -d 10.10.10.0/255.255.255.0 -j DROP
iptables -A FORWARD -i uap0 -s 10.10.10.0/255.255.255.0 -j ACCEPT
iptables -A FORWARD -i ppp0 -d 10.10.10.0/255.255.255.0 -j ACCEPT

iptables -A INPUT -i eth0 -p tcp -m tcp --dport 9051 -j DROP
iptables -A INPUT -i ppp0 -p tcp -m tcp --dport 9051 -j DROP
iptables -A INPUT -i eth0 -p udp -m udp --dport 0:1023 -j DROP
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 0:1023 -j DROP
iptables -A INPUT -i ppp0 -p udp -m udp --dport 0:1023 -j DROP
iptables -A INPUT -i ppp0 -p tcp -m tcp --dport 0:1023 -j DROP

# Re-enable bluetooth. In the earlier case, it didn't find the firmware.
#rmmod libertas_sdio libertas btmrvl_sdio btmrvl bluetooth 2>/dev/null
rmmod btmrvl_sdio btmrvl
/etc/init.d/bluetooth start

modprobe btmrvl_sdio
hciconfig hci0 up
hciconfig hci0 piscan
/usr/bin/mute-agent &

# Set leds
echo 1 > `eval ls /sys/class/leds/*plug*\:green\:health/brightness`
echo 1 > `eval ls /sys/class/leds/*plug*\:green\:wmode/brightness`
