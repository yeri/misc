#!/usr/bin/awk -f

BEGIN \
	{
		opentag=0
		header=0
	}

/---/ \
	{
		header++
	}

/\<[a-z].*\>/ && !/\<br \/\>/ \
	{
		if(header == 2)
			{
				opentag++
				# print "Opened tag on line: "NR, $0
			}
	}

/\<\/[a-z].*\>/ && !/\<br \/\>/ \
	{
		if(header == 2)
			{
				opentag--
				# print "Closed tag on line: "NR, $0
			}
	}

{
	if(header == 2)
		{
			if(opentag == 0 && $0 !~ /^$/ && $0 !~ /\<\/[a-z].*\>/ && $0 !~ /---/)
				{
					print "<p>"$0"</p>"
				}
			else
				{
					print $0
				}
		}
	else
		{
			print $0
		}

}

END \
	{
		# print "Open tags:\t"opentag
	}
