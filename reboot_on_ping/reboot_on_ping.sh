#!/bin/bash

device="$1"  # Get the device name from the first command-line argument

if [ -z "$device" ]; then
	echo "Usage: $0 <device_name>"
	echo "Use FQDN (eagle-XXXXXX.vpn.flatturtle-dns.com) for TurtleBoxes"
	exit 1  # Exit with an error if no device name is provided
fi

if [[ $device == eagle-* ]]; then  # Check if $device starts with "eagle-"
	device="${device}.vpn.flatturtle-dns.com"  # Append the rest of the domain
fi

echo ">> Monitoring $device. If it pings, I reboot..."
echo ""

while true; do
	if ping -c 1 "$device" &> /dev/null; then  # Attempt a single ping
		echo ""
		echo "$device responded to ping. Rebooting..."
		echo ""
		ssh "$device" reboot  # Initiate the SSH reboot command
		break  # Exit the loop after reboot
	else
		echo -n "."  # Print a dot to indicate ongoing monitoring
	fi

	sleep 1  # Wait for a second before the next ping attempt
done
