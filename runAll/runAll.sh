#!/bin/bash

if [ $# -eq 0 ]; then
	echo "No parameters were given."
	exit 1
fi

hosts=(`cat ~/git/misc/hosts/hosts`)

for host in "${hosts[@]}"; do
	echo "> $host"
	echo ""
	ssh "$host" "$@"
	echo ""
done