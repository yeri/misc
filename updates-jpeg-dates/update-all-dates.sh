#!/bin/bash

# Navigate to the folder containing the images
cd /Users/yeri/Downloads/LittleLives

# Loop through each .jpg file
for file in *.jpg; do
	# Extract date and type (in/out) from the filename
	filename=$(basename "$file")
	date_part=$(echo "$filename" | cut -d'-' -f1-3)
	type_part=$(echo "$filename" | grep -oE 'in|out')

	# Set the time based on the type
	if [ "$type_part" == "in" ]; then
		time="10:00:00"
	elif [ "$type_part" == "out" ]; then
		time="18:00:00"
	else
		echo "Skipping unknown file type: $filename"
		continue
	fi

	# Combine the date and time
	datetime="${date_part}T${time}"

	# Update the file's modified and created dates using exiftool
	exiftool -AllDates="$datetime" -overwrite_original "$file"

	echo "Updated $file with datetime: $datetime"
done