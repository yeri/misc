#!/bin/bash
# yeri
# 

# in  this example it's connecting to the sql server rr.sql.miiatech.com
#	user: proxy,  password: proxy,  database: proxy,  table: host,  row name: hostname.
#	-q for quick, and no buffering, and awk out the result (hostname, either server0 or server1).
echo -n "`date +%H:%M`  " >> list.txt
mysql -u proxy -pproxy -D proxy -e "select hostname from host" -h rr.sql.miiatech.com -q --unbuffered | awk 'NR==2 { print $1 }' >> list.txt

# keep 1008 last lines (run every 10 mins, keep 7 days)
tail -n 1008 list.txt > /tmp/_p_tmp.txt
mv /tmp/_p_tmp.txt list.txt

SQL0=`grep 0.sql.miiatech.com list.txt | wc -l`
SQL1=`grep 1.sql.miiatech.com list.txt | wc -l`
echo "0.sql.miiatech.com  $SQL0" > plot.txt
echo "1.sql.miiatech.com  $SQL1" >> plot.txt

cat plot.conf | gnuplot

mv rr.sql.miiatech.com.png ../img/

# 
#echo "`awk 'seen[$1]++ == 1 {print $1}' FS=":" list.txt`  `grep 0.sql.miiatech.com list.txt | wc -l`  `grep 1.sql.miiatech.com list.txt | wc -l`" > /tmp/_sql_list.txt
