#!/bin/bash
# Yeri

# Lang var
# nl, fr, de, en, ...
LANG=("de" "fr" "nl" "en")

# file with list with .txt extention
FILE=language-cv.txt

# file with CORRECT extention (.doc)
IN=/tmp/language-cv-doc.txt

# output dir (to cp files to) // folder has to exist
OUT=output

# TMP file
TMP=/tmp/list_c.txt

# i & j
i=0

# change .txt to .doc ...
sed s/txt/doc/ $FILE > $IN

# !!!!!
# clear $OUT dir
rm $OUT/* -r

# teh codez
for (( j=0; j<${#LANG[@]}; j++ ));
do
	echo "> Generating ${LANG[$j]}"
	mkdir -p $OUT/${LANG[$j]}
	grep ${LANG[$j]} $IN | awk '{ print $2 }' | sed s/CVs/./ | tr '\' '/' > $TMP

	# convert to unix txt file
	dos2unix $TMP

	while read a 
	do {
	        let i++
		echo "	> ($i) copying $a at `date -u +%H:%M:%S`"
	        cp $a $OUT/${LANG[$j]}
	} done < $TMP	
done
