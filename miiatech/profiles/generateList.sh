#!/bin/bash
# Yeri

# Lang var
# nl, fr, de, en, ...
LANG=("de" "fr" "nl" "en")

# dir with profiles
DIR=profiles-adecco

# file with list with .txt extention
FILE=profile-list.txt

# output dir (to cp files to) // folder has to exist
OUT=output

# TMP file
TMP=/tmp/list_p.txt

# i
i=0

# !!!!!
# clear $OUT dir
rm $OUT/* -r

# teh codez
for (( j=0; j<${#LANG[@]}; j++ ));
do
	echo "> Generating ${LANG[$j]}"
	mkdir -p $OUT/${LANG[$j]}
	grep ${LANG[$j]} $FILE | awk '{ print $1 }' > $TMP

	# convert to unix txt file
	dos2unix $TMP

	while read a 
	do {
	        let i++
		echo "	> ($i) copying $a at `date -u +%H:%M:%S`"
	        cp $DIR/$a/$a.txt $OUT/${LANG[$j]}
	} done < $TMP	
done

