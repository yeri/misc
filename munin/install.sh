#!/bin/bash

echo ">> Installing docker munin."

apt install python3-pip -y
pip install docker

cp plugins/docker_ /usr/share/munin/plugins/
cp plugin-conf.d/docker.conf /etc/munin/plugin-conf.d/

ln -s /usr/share/munin/plugins/docker_ /etc/munin/plugins/docker_containers
ln -s /usr/share/munin/plugins/docker_ /etc/munin/plugins/docker_cpu
ln -s /usr/share/munin/plugins/docker_ /etc/munin/plugins/docker_images
ln -s /usr/share/munin/plugins/docker_ /etc/munin/plugins/docker_network
ln -s /usr/share/munin/plugins/docker_ /etc/munin/plugins/docker_status

systemctl restart munin-node