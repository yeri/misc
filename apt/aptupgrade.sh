#!/bin/bash

hosts=(`cat ~/git/misc/hosts/hosts`)

if [[ $1 = "" ]]; then

	for i in "${hosts[@]}"
	do :
		echo "	>> host: $i"
		echo "	>> ($i) running apt update"
		ssh -q -o ConnectTimeout=2 -o ConnectionAttempts=1 $i DEBIAN_FRONTEND=noninteractive apt-get update
		echo ""

		# SSH into the remote machine and check if /usr/bin/cloudflared exists
		ssh -q $i "[ -f /usr/bin/cloudflared ]" &> /dev/null
		# Check the exit code of the previous command
		if [ $? -eq 0 ]; then
			echo "	>> ($i) cloudflared is installed."
			# Store the initial modified time of the file
			initial_modified_time=$(ssh $i "stat -c %Y /usr/bin/cloudflared")

			echo "	>> ($i) running apt upgrade"
			ssh -q -o ConnectTimeout=2 -o ConnectionAttempts=1 $i DEBIAN_FRONTEND=noninteractive apt-get upgrade -y

			# Check the modified time of the file again
			current_modified_time=$(ssh $i "stat -c %Y /usr/bin/cloudflared")

			# Compare the modified times
			if [[ $current_modified_time -gt $initial_modified_time ]]; then
				# The file has been modified, restart cloudflared
				ssh $i "systemctl restart cloudflared"
				echo "	>> ($i) cloudflared has been restarted."
			else
				echo "	>> ($i) cloudflared has not been updated."
			fi
		else
			echo "	>> ($i) running apt upgrade"
			ssh -q -o ConnectTimeout=2 -o ConnectionAttempts=1 $i DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
		fi

		ssh -q -o ConnectTimeout=2 -o ConnectionAttempts=1 $i "if command -v tailscale > /dev/null 2>&1; then echo -e \"\n\t>> ($i) running apt install tailscale\" && DEBIAN_FRONTEND=noninteractive apt-get install tailscale -y ; fi"
		echo ""
		echo "	>> ($i) running apt autoremove"
		ssh -q -o ConnectTimeout=2 -o ConnectionAttempts=1 $i DEBIAN_FRONTEND=noninteractive apt-get autoremove -y
		echo ""
		echo "	>> ($i) running omz update if installed"
		ssh -q -o ConnectTimeout=2 -o ConnectionAttempts=1 $i "test -d /root/.oh-my-zsh && cd /root/.oh-my-zsh && /root/.oh-my-zsh/tools/upgrade.sh"
		echo ""
		echo "	>> done for $i"
		echo ""
	done
else
	echo "	>> host: $1"
	echo "	>> ($1) running apt update"
	ssh $1 DEBIAN_FRONTEND=noninteractive apt-get update
	echo ""
	echo "	>> ($1) running apt upgrade"
	ssh $1 DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
	echo ""
	echo "	>> ($1) running apt install tailscale"
	ssh $1 'if tailscale > /dev/null 2>&1; then DEBIAN_FRONTEND=noninteractive apt-get install tailscale -y ; else echo "Tailscale not running on this system." ; fi'
	echo ""
	echo "	>> ($1) running apt autoremove"
	ssh $1 DEBIAN_FRONTEND=noninteractive apt-get autoremove -y
	echo "	>> done for $1"
	echo ""
fi