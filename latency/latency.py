#!/usr/bin/env python3

import subprocess
import threading
from queue import Queue
import platform

# Function to find the IPv4 gateway IP address
def find_gateway():
	try:
		if platform.system() == "Linux":
			result = subprocess.run(
				["ip", "route"],
				capture_output=True, text=True
			)
			for line in result.stdout.splitlines():
				if line.startswith("default"):
					return line.split()[2]
		elif platform.system() == "Darwin":
			result = subprocess.run(
				["netstat", "-rn"],
				capture_output=True, text=True
			)
			for line in result.stdout.splitlines():
				if "default" in line and "." in line:
					return line.split()[1]
	except Exception as e:
		print(f"Error finding gateway: {str(e)}")
		return None

# Function to perform ping and extract average time
def ping_host(queue, domain, name):
	try:
		result = subprocess.run(
			["ping", "-c", "5", domain],
			capture_output=True, text=True
		)
		if result.returncode == 0:
			output = result.stdout
			avg_time_line = next(
				(line for line in output.splitlines() if "avg" in line), None
			)
			if avg_time_line:
				avg_time = avg_time_line.split('/')[4].replace("ms", "")
				queue.put((name, avg_time))
			else:
				queue.put((name, "timeout"))
		else:
			queue.put((name, "timeout"))
	except Exception as e:
		queue.put((name, f"error: {str(e)}"))

# Main function to start pinging
def main():
	hosts = [
		("google.com", "Google"),
		("cloudflare.com", "Cloudflare"),
		("apple.com", "Apple"),
		("facebook.com", "Facebook"),
		("dns1.nextdns.io", "NextDNS dns1"),
		("dns2.nextdns.io", "NextDNS dns2"),
		("sg.yeri.be", "sg.yeri.be"),
		("be.yeri.be", "be.yeri.be"),
		("rootspirit.com", "Rootspirit"),
		("liana.ts.yeri.be", "Liana"),
		("mammoth.ts.yeri.be", "Mammoth"),
		("tyr.ts.yeri.be", "Tyr"),
		("industry.ts.yeri.be", "Industry"),
		("grima.ts.yeri.be", "Grima"),
	]

	# Find the gateway and add it to the hosts list
	gateway_ip = find_gateway()
	if gateway_ip:
		hosts.insert(0, (gateway_ip, f"Gateway ({gateway_ip})"))

	queue = Queue()
	threads = []

	# Start threads for each host
	for domain, name in hosts:
		thread = threading.Thread(target=ping_host, args=(queue, domain, name))
		thread.start()
		threads.append(thread)

	# Wait for all threads to finish
	for thread in threads:
		thread.join()

	# Retrieve results from the queue and print them
	results = []
	while not queue.empty():
		results.append(queue.get())

	# Sort results alphabetically by name, case-insensitive
	results.sort(key=lambda x: x[0].lower())

	# Print sorted results
	for name, latency in results:
		print(f"{name:<25}\t{latency}")

if __name__ == "__main__":
	main()