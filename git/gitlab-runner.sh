#!/bin/bash

# gitlab runner will fail unless you change /etc/gitlab-runner/config.toml
# change volume to
# volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
# https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1986

# Download files
AMD64="https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
RPI64="https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_aarch64.deb"

DLPATH="/tmp/gitlab-runner.deb"
ARCH="$(uname -m)"

if [ $ARCH == "x86_64" ]; then
	DOWNLOAD=$AMD64
elif  [ $ARCH == "aarch64" ]; then
	DOWNLOAD=$RPI64
else
	echo "Not sure which arch we're on. Stopping."
	return 1
fi

wget $DOWNLOAD -O $DLPATH
dpkg -i $DLPATH
rm -f $DLPATH