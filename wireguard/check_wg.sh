#!/bin/bash

# Get the latest handshake time in seconds
latest_handshake=$(wg show wg0 latest-handshakes | awk '{print $2}')
current_time=$(date +%s)

# Calculate the difference in time
time_diff=$((current_time - latest_handshake))

# If the difference is more than 600 seconds, restart the interface
if [ $time_diff -gt 600 ]; then
	wg-quick down wg0
	wg-quick up wg0
fi