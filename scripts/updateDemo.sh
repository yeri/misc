#!/bin/bash
cd /srv/flatturtle/Projects/InfoScreen
git checkout demo
git pull
cp /srv/flatturtle/demo.flatturtle.com/config.php /tmp/._config
cp /srv/flatturtle/demo.flatturtle.com/.htaccess /tmp/._htaccess
rm -r /srv/flatturtle/demo.flatturtle.com/*
cp -a * /srv/flatturtle/demo.flatturtle.com/
mv /tmp/._config /srv/flatturtle/demo.flatturtle.com/config.php
mv /tmp/._htaccess /srv/flatturtle/demo.flatturtle.com/.htaccess
git checkout master
git pull
cd -
if [[ $EUID -eq 0 ]]; then
        chown -R flatturtle:flatturtle /srv/flatturtle/Projects/InfoScreen
	chown -R flatturtle:flatturtle /srv/flatturtle/demo.flatturtle.com/
fi
