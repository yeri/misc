#!/bin/bash
# Bind MRTG stats
# by Yeri Tiete (Tuinslak) - 10/02/2011
# http://yeri.be
#
# mrtg.cfg sample:
################################################################################
#	#
#	# Bind
#	#
#	Title[bind]: Bind Queries
#	Target[bind]: `/etc/mrtg/bind-stats.sh`
#	PageTop[bind]: <H1> Bind queries per minute on vm1 </H1>
#	Options[bind]: growright,pngdate,nobanner,gauge,nopercent,noinfo
#	MaxBytes[bind]: 50000
#	Ylegend[bind]: Queries/min
#	ShortLegend[bind]:  queries/min
#	LegendO[bind]: Incoming Bind queries per minute
#	LegendI[bind]: Outcoing Bind queries per minute
#	Legend2[bind]: Incoming Bind queries per minute
#	Legend1[bind]: Outcoing Bind queries per minute
################################################################################

# file path of named.stats
FILE=/var/log/named.stats
TMPFILE=/tmp/__dnsstats.txt

# how often does mrtg run? for me it's every 10 mins
TIME=10

# make file empty
echo /dev/null > /var/log/named.stats

# generate file
/usr/sbin/rndc stats

# save number of queries
INNOW=`egrep "[^I]QUERY" $FILE | awk '{print $1 }'`
OUTNOW=`grep Outgoing $FILE -A 9 | sed '1,2d' | awk '{ SUM += $1} END { print SUM }'`

# check if tmp file exists and insert data in it if it doesnt
# this prevents a peak
[ ! -e $TMPFILE ] && echo $INNOW > $TMPFILE && echo $OUTNOW >> $TMPFILE

# get old data
INOLD=`cat $TMPFILE | sed -n 1p`
OUTOLD=`cat $TMPFILE | sed -n 2p`

# overwrite old
echo $INNOW > $TMPFILE
echo $OUTNOW >> $TMPFILE

# calculate (to get difference)
INDIFF=$[ $INNOW-$INOLD ]
OUTDIFF=$[ $OUTNOW-$OUTOLD ]

# as mrtg runs */10 > divide by 10 to get per minute
INPERMIN=$[ $INDIFF/$TIME ]
OUTPERMIN=$[ $OUTDIFF/$TIME ]

# print !
echo $OUTPERMIN
echo $INPERMIN
echo
echo
