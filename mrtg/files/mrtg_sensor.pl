#!/usr/bin/perl -w
# July 9, 2003 By Jesper Knudsen (jkn@swerts-knudsen.dk)
#        
# usage: $0 lm sensor to get MRTG output from host
#
# returns the value in MRTG format for the lm sensor specified
#
# use perl;
  use strict;

  my %DISP; $DISP{'systemp'} = 0;

  if ( $#ARGV != 0 ){
        die "\nUsage: $0 lm_sensor\n";
  };


  open SENSORS, "sensors >&1|";
    while (my $LINE = <SENSORS>) {
      my (@TMP); @TMP = split /[\:\ ]+/, $LINE;

      if ($TMP[0] =~ $ARGV[0]) {
        ($DISP{'systemp'}) = ($TMP[1] =~ m/([0-9]+)/);
      }

  }
  close SENSORS;

  print "$DISP{'systemp'}\n$DISP{'systemp'}\n\n\n\n";

  exit 0;
