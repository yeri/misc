#!/usr/bin/env bash

echo ""
echo "########################"
echo "#        OCEAN         #"
echo "########################"
echo ""

ssh ocean 'docker stop ocean-nginx
certbot renew --no-random-sleep --standalone
docker start ocean-nginx
'

#echo ""
#echo "########################"
#echo "#        LIANA         #"
#echo "########################"
#echo ""

#ssh liana 'docker stop liana-nginx
#certbot renew --no-random-sleep --standalone
#docker start liana-nginx
#'

echo ""
echo "########################"
echo "#        NIMBUS        #"
echo "########################"
echo ""

ssh nimbus 'certbot renew --nginx --no-random-sleep
/etc/init.d/nginx reload
'

echo ""
echo "########################"
echo "#        MAIEV         #"
echo "########################"
echo ""

ssh maiev '/etc/init.d/apache2 stop
certbot renew --standalone
/etc/init.d/apache2 restart
'

echo ""
echo "########################"
echo "#     STUFFMASTER      #"
echo "########################"
echo ""

ssh stuffmaster '/etc/init.d/nginx stop
certbot renew --no-random-sleep
/etc/init.d/nginx restart
'

# copy key to nimbus for proxy shizzle

echo ">> to Nimbus - flatturtle.xyz"
ssh nimbus mkdir -p /etc/letsencrypt/live/flatturtle.xyz/
scp stuffmaster:/etc/letsencrypt/live/flatturtle.xyz/privkey.pem /tmp/privkey.pem
scp stuffmaster:/etc/letsencrypt/live/flatturtle.xyz/fullchain.pem /tmp/fullchain.pem
scp /tmp/privkey.pem nimbus:/etc/letsencrypt/live/flatturtle.xyz/privkey.pem
scp /tmp/fullchain.pem nimbus:/etc/letsencrypt/live/flatturtle.xyz/fullchain.pem
rm /tmp/privkey.pem
rm /tmp/fullchain.pem

echo ">> to Nimbus - screenshots.flatturtle.xyz"
ssh nimbus mkdir -p /etc/letsencrypt/live/screenshots.flatturtle.xyz/
scp stuffmaster:/etc/letsencrypt/live/screenshots.flatturtle.xyz/privkey.pem /tmp/privkey.pem
scp stuffmaster:/etc/letsencrypt/live/screenshots.flatturtle.xyz/fullchain.pem /tmp/fullchain.pem
scp /tmp/privkey.pem nimbus:/etc/letsencrypt/live/screenshots.flatturtle.xyz/privkey.pem
scp /tmp/fullchain.pem nimbus:/etc/letsencrypt/live/screenshots.flatturtle.xyz/fullchain.pem
rm /tmp/privkey.pem
rm /tmp/fullchain.pem

echo ">> to Nimbus - sentry.flatturtle.xyz"
ssh nimbus mkdir -p /etc/letsencrypt/live/sentry.flatturtle.xyz/
scp stuffmaster:/etc/letsencrypt/live/sentry.flatturtle.xyz/privkey.pem /tmp/privkey.pem
scp stuffmaster:/etc/letsencrypt/live/sentry.flatturtle.xyz/fullchain.pem /tmp/fullchain.pem
scp /tmp/privkey.pem nimbus:/etc/letsencrypt/live/sentry.flatturtle.xyz/privkey.pem
scp /tmp/fullchain.pem nimbus:/etc/letsencrypt/live/sentry.flatturtle.xyz/fullchain.pem
rm /tmp/privkey.pem
rm /tmp/fullchain.pem

echo ">> to Nimbus - docs.flatturtle.xyz"
ssh nimbus mkdir -p /etc/letsencrypt/live/docs.flatturtle.xyz/
scp stuffmaster:/etc/letsencrypt/live/docs.flatturtle.xyz/privkey.pem /tmp/privkey.pem
scp stuffmaster:/etc/letsencrypt/live/docs.flatturtle.xyz/fullchain.pem /tmp/fullchain.pem
scp /tmp/privkey.pem nimbus:/etc/letsencrypt/live/docs.flatturtle.xyz/privkey.pem
scp /tmp/fullchain.pem nimbus:/etc/letsencrypt/live/docs.flatturtle.xyz/fullchain.pem
rm /tmp/privkey.pem
rm /tmp/fullchain.pem

echo ""
echo "########################"
echo "#         UNMS         #"
echo "########################"
echo ""

ssh unms '/etc/init.d/nginx stop
certbot renew --no-random-sleep
/etc/init.d/nginx restart
'

echo ">> to Nimbus - unms.flatturtle.xyz"
ssh nimbus mkdir -p /etc/letsencrypt/live/unms.flatturtle.xyz/
scp unms:/etc/letsencrypt/live/unms.flatturtle.xyz/privkey.pem /tmp/privkey.pem
scp unms:/etc/letsencrypt/live/unms.flatturtle.xyz/fullchain.pem /tmp/fullchain.pem
scp /tmp/privkey.pem nimbus:/etc/letsencrypt/live/unms.flatturtle.xyz/privkey.pem
scp /tmp/fullchain.pem nimbus:/etc/letsencrypt/live/unms.flatturtle.xyz/fullchain.pem
rm /tmp/privkey.pem
rm /tmp/fullchain.pem

# copy key to vm-dns-mail

echo ""
echo "########################"
echo "#      VM-DNS-MAIL     #"
echo "########################"
echo ""

ssh vm-dns-mail '/etc/init.d/apache2 stop
certbot renew --no-random-sleep
/etc/init.d/apache2 restart
'

# copy key for mail
ssh vm-dns-mail 'cp /etc/letsencrypt/live/webmail.rootspirit.com/privkey.pem /tmp/webmail.privkey.pem
cp /etc/letsencrypt/live/webmail.rootspirit.com/fullchain.pem /tmp/webmail.fullchain.pem
cat /tmp/webmail.privkey.pem /tmp/webmail.fullchain.pem > /tmp/webmail.all.pem
cp /tmp/webmail.privkey.pem /etc/ssl/letsencrypt/webmail.privkey.pem
cp /tmp/webmail.fullchain.pem /etc/ssl/letsencrypt/webmail.fullchain.pem
cp /tmp/webmail.all.pem /etc/ssl/letsencrypt/webmail.all.pem
rm /tmp/webmail.privkey.pem
rm /tmp/webmail.fullchain.pem
rm /tmp/webmail.all.pem
'
ssh vm-dns-mail /etc/init.d/postfix restart
ssh vm-dns-mail /etc/init.d/courier-imap-ssl restart
ssh vm-dns-mail /etc/init.d/courier-pop-ssl restart


# copy key to nimbus for proxy shizzle
echo ">> to Nimbus - webmail.rootspirit.com"
ssh nimbus mkdir -p /etc/letsencrypt/live/webmail.rootspirit.com/
scp vm-dns-mail:/etc/letsencrypt/live/webmail.rootspirit.com/privkey.pem /tmp/privkey.pem
scp vm-dns-mail:/etc/letsencrypt/live/webmail.rootspirit.com/fullchain.pem /tmp/fullchain.pem
scp /tmp/privkey.pem nimbus:/etc/letsencrypt/live/webmail.rootspirit.com/privkey.pem
scp /tmp/fullchain.pem nimbus:/etc/letsencrypt/live/webmail.rootspirit.com/fullchain.pem
rm /tmp/privkey.pem
rm /tmp/fullchain.pem
ssh nimbus /etc/init.d/nginx reload

echo ""
echo "########################"
echo "#   Unifi controllers  #"
echo "########################"
echo ""

ssh ui0 'certbot renew --nginx --no-random-sleep
/etc/init.d/nginx reload
'

# Reload nimbus nginx to make sure it has latest certs loaded.
ssh nimbus /etc/init.d/nginx reload

# EOF
