import os
import smtplib
from email.mime.text import MIMEText
from dotenv import load_dotenv

# Load environment variables from .env file
load_dotenv()

# Fetch SMTP credentials from the environment
SMTP_SERVER = os.getenv('SMTP_SERVER')
SMTP_PORT = int(os.getenv('SMTP_PORT', 587))  # Default to 587 if not specified
SMTP_USERNAME = os.getenv('SMTP_USERNAME')
SMTP_PASSWORD = os.getenv('SMTP_PASSWORD')
SENDER_EMAIL = os.getenv('SENDER_EMAIL')
TEST_RECIPIENT = "yeri@flatturtle.com"

# Test email content
subject = "SMTP Test Email"
body = "This is a test email to confirm SMTP credentials are working."

def send_test_email():
	try:
		# Create a text email
		msg = MIMEText(body)
		msg['Subject'] = subject
		msg['From'] = SENDER_EMAIL
		msg['To'] = TEST_RECIPIENT

		# Connect to the SMTP server
		with smtplib.SMTP(SMTP_SERVER, SMTP_PORT) as server:
			server.starttls()  # Upgrade the connection to secure
			server.login(SMTP_USERNAME, SMTP_PASSWORD)
			server.sendmail(SENDER_EMAIL, TEST_RECIPIENT, msg.as_string())
			print("Test email sent successfully.")
	except Exception as e:
		print(f"Failed to send test email: {e}")

if __name__ == "__main__":
	send_test_email()